#####============= Prepare Dataset
from imutils import paths
import cv2
import os
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
imagePaths = sorted(list(paths.list_images("dataset1")))
X_train = []

for imagePath in imagePaths:
	# load the image, pre-process it, and store it in the data list
	image = cv2.imread(imagePath)
	image = cv2.resize(image, (704, 704))
	# image = img_to_array(image)
	X_train.append(image)
Y_train=[]
imagePaths1 = sorted(list(paths.list_images("masks1")))
for imagePath in imagePaths1:
    mask = cv2.imread(imagePath)
    mask = cv2.resize(mask, (704, 704))
    mask = img_to_array(mask)
    Y_train.append(mask)


import numpy as np
data = np.array(X_train, dtype="float") / 255.0
data2 = np.array(Y_train, dtype="float") / 255.0
# labels = np.array(labels)

# partition the data into training and testing splits using 75% of
# the data for training and the remaining 25% for testing
(trainX, testX, trainY, testY) = train_test_split(data,
                data2, test_size=0.25, random_state=42)

# convert the labels from integers to vectors
# trainY = to_categorical(trainY, num_classes=2)
# testY = to_categorical(testY, num_classes=2)

from keras.preprocessing.image import ImageDataGenerator
aug = ImageDataGenerator(
          rescale=1./255,
          rotation_range=360,
          width_shift_range=0.2,
          height_shift_range=0.2,
          shear_range=0.2,
          zoom_range=0.2,
          horizontal_flip=True,
          fill_mode='nearest')
####============= Creating CNN Model ===========================================
###------ XToad Net

from keras import Input


from keras import applications
from keras.layers import *
from keras.models import *

num_classes=1
input_shape = (704, 704, 3)
in1 = Input(input_shape) # we creat tensor to feed the CNN
base_model =applications.Xception(include_top=False, input_tensor=in1, pooling=None)
# base_model =applications.Xception()
base_model.summary()

x1 = base_model.layers[-1].output
x1 = GlobalAveragePooling2D()(x1)
x1 = Dropout(0.5)(x1)
x1 = Dense(num_classes)(x1)
x1 = Activation('sigmoid')(x1)
MyModel = Model(inputs=[in1], outputs=[x1])
MyModel.summary()

## Compiling
from keras import optimizers
from keras import losses
MyModel.compile(loss="binary_crossentropy" ,
                  optimizer=optimizers.Adam(lr=.0001),
                  metrics=['acc'])

####============Preprocessing Data =================

# train the network
print("[INFO] training network...")
H = MyModel.fit_generator(aug.flow(trainX, trainY, batch_size=32),
	validation_data=(testX, testY), steps_per_epoch=len(trainX) // 32,
	epochs=2, verbose=1)

print('FINIIIIISSSSSSSSHHHH')
