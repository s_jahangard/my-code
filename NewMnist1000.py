# -*- coding: utf-8 -*-
"""
Created on Sun Sep  2 14:05:11 2018

@author: Simin
This code create New mnist Data with different size and font
To use your desired font put them on the Fonts folder
"""
import matplotlib.pyplot as plt
import numpy as np
from PIL import  Image, ImageDraw , ImageFont
import random
import scipy.misc
import os, os.path
def FontNames(path):
    from PIL import Image
    import os, os.path
    FontNames1 = []
    valid_images = [".ttf"]
    for f in os.listdir(path):
       ext = os.path.splitext(f)[1]
       if ext.lower() not in valid_images:
          continue
       namefont = os.path.splitext(f)[0]
       FontNames1.append(namefont)
    return FontNames1
W=28
H=28
path = "D:/DeepLearning/Mnist/Fonts"
Lid=FontNames(path)
print('Creating Images')

for i in range(100000):
   img = Image.new('RGB', (W, H), color = 'black')
   msg=str(random.randint(0,9))
   FontSize=random.randint(8,22)
   FontStyle=Lid[random.randint(1,len(Lid)-1)]
   fnt = ImageFont.truetype('Fonts/'+'{0}'.format(FontStyle)+'.ttf', FontSize)
   d = ImageDraw.Draw(img)
   w, h = d.textsize(msg)
   d.text(((W-w)/2,(H-h)/2),msg,font=fnt, fill=(255, 255, 255))
#   plt.imshow(img)
#   plt.show()
#   img.save('D:/DeepLearning/Mnist/1000testpic/'+'{0}'.format(msg)+".jpg")
   scipy.misc.imsave('D:/DeepLearning/Mnist/1000testpic/'+str(i)+'_'+'{0}'.format(msg)+'.jpg', img)
   print(i)
print('finish')