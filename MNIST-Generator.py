# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 15:42:01 2018

@author: Simin
"""
from keras.preprocessing.image import ImageDataGenerator
from keras import Input
from keras import applications
from keras.layers import *
from keras.models import *
from keras.models import Model
from keras.layers import Dense, Activation, Dropout,GlobalAveragePooling2D
from keras.utils import np_utils
import matplotlib.image as mpimg
import pandas as pd

####===========  Agumentation
aug = ImageDataGenerator(rotation_range=20, zoom_range=0.15,
width_shift_range=0.2, height_shift_range=0.2, shear_range=0.15,
horizontal_flip=False, fill_mode="nearest")
###=====  Calling My Generator
dbPath_train='D:/DeepLearning/Mnist/my_csv_train.csv'
dbPath_valid='D:/DeepLearning/Mnist/my_csv_valid.csv'
batchSize=32
from DatasetGenerator import DatasetGenerator
trainGen = DatasetGenerator(dbPath_train, batchSize, aug=aug,classes=10)
valGen = DatasetGenerator(dbPath_valid, batchSize, aug=aug,classes=10)

####=============== Creating Model =======================

num_classes=10
input_shape=(28,28,3)
in1 = Input(input_shape) # we creat tensor to feed the CNN
base_model =applications.Xception(include_top=False, input_tensor=in1, pooling=None)
#base_model =applications.Xception()
base_model.summary()

x1 = base_model.layers[-1].output
x1 = GlobalAveragePooling2D()(x1)
x1 = Dropout(0.5)(x1)
x1 = Dense(num_classes)(x1)
x1 = Activation('softmax')(x1)
MyModel = Model(inputs=[in1], outputs=[x1])
MyModel.summary()
import keras
MyModel.compile(optimizer=keras.optimizers.Adam(), loss=keras.losses.categorical_crossentropy, metrics=['accuracy'])
######=========================================================
#########=====================Training our model
print('training....')
MyModel.fit_generator(
      trainGen.generator(),
      steps_per_epoch=trainGen.numImages // batchSize,
      validation_data=valGen.generator(),
      validation_steps=valGen.numImages // batchSize,
      epochs=75,
      max_queue_size=batchSize * 2)
print('training finish')
