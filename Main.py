
import plot_net
import pandas as pd
data_train=pd.read_csv('D:/DeepLearning/Mnist/all/train.csv')
rowsNum1,colsNum1=data_train.shape

#train_labels=[]
train_labels=data_train.values[:rowsNum1,0]
train_X=data_train.values[:rowsNum1,1:]


data_test=pd.read_csv('D:/DeepLearning/Mnist/all/test.csv')
rowsNum2,colsNum2=data_test.shape
test_X=data_test.values[:rowsNum2,0:]

x_train = train_X.reshape(-1, 28, 28, 1).astype('float32') / 255.
x_test = test_X.reshape(-1, 28, 28, 1).astype('float32') / 255.
   
train_labels=list(train_labels)
from keras.utils import np_utils
y_train=np_utils.to_categorical(train_labels)

from sklearn.model_selection import train_test_split
random_seed = 2
X_train, X_val, Y_train, Y_val = train_test_split(x_train, y_train, test_size = 0.1, random_state=random_seed)


### Creating Model by Functional API
from keras import layers
from keras.layers import Dense
input=layers.Input(shape=(28,28,1))
Conv1=layers.Conv2D(16,(3,3),activation='relu',strides=2,padding='same')(input)
Conv2=layers.Conv2D(32,(3,3),activation='relu',strides=2,padding='same')(Conv1)
flat=layers.Flatten()(Conv2)
Out_put=Dense(10, activation='softmax')(flat)
from keras.models import Model
MyModel=Model(input,Out_put)

MyModel.summary()
import keras
MyModel.compile(optimizer=keras.optimizers.Adam(), loss=keras.losses.categorical_crossentropy, metrics=['accuracy'])
from keras.preprocessing.image import ImageDataGenerator 
### Training our model
datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        rotation_range=10,  # randomly rotate images in the range (degrees, 0 to 180)
        zoom_range = 0.1, # Randomly zoom image 
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=False,  # randomly flip images
        vertical_flip=False) 
datagen.fit(x_train)
print('training....')
batch_size=32
Network_history=MyModel.fit_generator(datagen.flow(X_train,Y_train, batch_size=batch_size),
                              epochs = 1, validation_data = (X_val,Y_val),
                              verbose = 2, steps_per_epoch=x_train.shape[0] // batch_size)
print('training finish')
plot_net(Network_history)
test_labels_p=MyModel.predict(x_test)
import numpy as np
test_labels_p=np.argmax(test_labels_p,axis=1)