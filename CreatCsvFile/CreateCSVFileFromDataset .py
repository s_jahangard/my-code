####====== Reading and Preparing Dataset in csv file

from PIL import Image
from imutils import paths
import pandas as pd
import csv
import numpy as np
new_width = 704
new_height = 704
imagePaths =list(paths.list_images("testimage"))
for image in imagePaths:
  im = Image.open(image)  # relative path to file
  # im.show()
  im = im.resize((new_width, new_height), Image.ANTIALIAS)
  img_grey = im.convert('L')
  # img_grey.show()
  c=img_grey.getdata()
  value = np.asarray(img_grey.getdata(), dtype=np.int).reshape((img_grey.size[1], img_grey.size[0]))
  value = value.flatten()
  print(value)

  with open("img_pixels.csv", 'a') as f:
    writer = csv.writer(f)
    writer.writerow(value)
