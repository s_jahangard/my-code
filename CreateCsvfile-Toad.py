# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 14:55:17 2018

@author: Simin
"""
from imutils import paths
import pandas as pd
import os

imagePaths1 =list(paths.list_images("D:\\DeepLearning\\ToadProject\\dataset1\\toad"))
imagePaths2 =list(paths.list_images("D:\\DeepLearning\\ToadProject\\dataset1\\not_toad"))

imagePaths1_M =list(paths.list_images("D:\\DeepLearning\\ToadProject\\masks1\\toad"))
imagePaths2_M =list(paths.list_images("D:\\DeepLearning\\ToadProject\\masks1\\not_toad"))

if len(imagePaths1)!=len(imagePaths1_M):
    print('Error.The Length must be Same!')
if len(imagePaths2)!=len(imagePaths2_M):
    print('Error.The Length must be Same!')

LabelToad=[]
LabelNotToad=[]
ImageNames1=[]
ImageNames2=[]
ImageNames1_M=[]
ImageNames2_M=[]
# count=0;
for i in range(len(imagePaths1)):
    t=imagePaths1[i]
    path1, file1 = os.path.split(t)
    label=path1.split("\\")[-1]
    LabelToad.append(label)
    ImageNames1.append(file1)

    t1=imagePaths1_M[i]
    path1_M, file1_M = os.path.split(t1)
    ImageNames1_M.append(file1_M)
    
    
for j in range(len(imagePaths2_M)):
    t=imagePaths2[j]
    path2, file2 = os.path.split(t)
    label=path2.split("\\")[-1]
    LabelNotToad.append(label)
    ImageNames2.append(file2)

    t1=imagePaths2_M[j]
    path2_M, file2_M = os.path.split(t1)
    ImageNames2_M.append(file2_M)

raw_data={'X_path':imagePaths1+imagePaths2 ,
          'X':ImageNames1+ImageNames2,
          'Y_path':imagePaths1_M+imagePaths2_M,
          'Y':ImageNames1_M+ImageNames2_M,
          'Label':LabelToad+LabelNotToad}
df = pd.DataFrame(raw_data,columns = ['X_path','X','Y_path','Y','Label'])
df.to_csv('my_csv_Toad_train.csv')
print("Writing complete")