
import plot_net
import pandas as pd
data_train=pd.read_csv('D:/DeepLearning/Mnist/all/train.csv')
rowsNum1,colsNum1=data_train.shape

#train_labels=[]
train_labels=data_train.values[:rowsNum1,0]
train_X=data_train.values[:rowsNum1,1:]


data_test=pd.read_csv('D:/DeepLearning/Mnist/all/test.csv')
rowsNum2,colsNum2=data_test.shape
test_X=data_test.values[:rowsNum2,0:]

x_train = train_X.reshape(-1, 28, 28, 1).astype('float32') / 255.
x_test = test_X.reshape(-1, 28, 28, 1).astype('float32') / 255.
   
train_labels=list(train_labels)
from keras.utils import np_utils
y_train=np_utils.to_categorical(train_labels)


### Creating Model by Functional API
from keras import layers
from keras.layers import Dense
input=layers.Input(shape=(28,28,1))
Conv1=layers.Conv2D(16,(3,3),activation='relu',strides=2,padding='same')(input)
Conv2=layers.Conv2D(32,(3,3),activation='relu',strides=2,padding='same')(Conv1)
flat=layers.Flatten()(Conv2)
Out_put=Dense(10, activation='softmax')(flat)
from keras.models import Model
MyModel=Model(input,Out_put)

MyModel.summary()
import keras
MyModel.compile(optimizer=keras.optimizers.Adam(), loss=keras.losses.categorical_crossentropy, metrics=['accuracy'])

### Training our model
print('training....')
Network_history=MyModel.fit(x_train,y_train,batch_size=128,epochs=20,validation_split=0.2)
print('training finish')
plot_net(Network_history)
test_labels_p=MyModel.predict(x_test)
import numpy as np
test_labels_p=np.argmax(test_labels_p,axis=1)