def plot_net(net_history):
    history=net_history.history
    import matplotlib.pyplot as plt
    Losses=history['loss']
    Accuracy=history['acc']
    Loss_val= history['val_loss']
    acc_val=history['val_acc']
    plt.xlabel('Epoches')
    plt.ylabel('loss')
    plt.plot(Losses)
    plt.plot(Loss_val)
    plt.legend('train', 'validation')
    plt.figure()
    plt.xlabel('Epoches')
    plt.ylabel('Accuracy')
    plt.plot(Accuracy)
    plt.plot(acc_val)
    plt.legend('train','validation')
