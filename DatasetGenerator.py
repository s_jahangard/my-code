# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 10:57:37 2018

@author: Simin
"""
# import the necessary packages
from keras.utils import np_utils
import matplotlib.image as mpimg
import pandas as pd
import numpy as np
class DatasetGenerator:
      def __init__(self, dbPath=None, batchSize=32,
           aug=None,binarize=True, classes=10):
# store the batch size, preprocessors, and data augmentor,
# whether or not the labels should be binarized, along with
# the total number of classes
           self.batchSize = batchSize
           self.aug = aug
           self.binarize = binarize
           self.classes = classes
           self.dbPath=dbPath
# open the .csv database for reading and determine the total
# number of entries in the database
           self.db = pd.read_csv(self.dbPath)
           self.db=pd.read_csv(self.dbPath)
           self.numImages=self.db.shape[0]
           self.train_labels=list(self.db.values[:self.numImages,3]) # read Labels
           self.Directory=self.db.values[:self.numImages,1]
           self.train_x=[]
           for j in self.Directory:
                self.image = mpimg.imread(j)
                self.train_x.append(self.image)
#          self.numImages = self.db["labels"].shape[0]
      def generator(self, passes=np.inf):
 # initialize the epoch count
           epochs = 0
 # keep looping infinitely -- the model will stop once we have
 # reach the desired number of epochs
           while epochs < passes:
 # loop over the .csv dataset
             for i in np.arange(0, self.numImages, self.batchSize):
 # extract the images and labels from the  dataset
                images = self.train_x[i: i + self.batchSize]
                labels = self.train_labels[i: i + self.batchSize]
                # check to see if the labels should be binarized
                if self.binarize:
                  labels = np_utils.to_categorical(labels,self.classes)
                if self.aug is not None:
                  (images, labels) = next(self.aug.flow(images,
                  labels, batch_size=self.batchSize))
                yield (images, labels)
           epochs=epochs+1
    # def close(self):
 # close the database
 #     self.db.close()