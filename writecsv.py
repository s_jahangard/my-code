"""

@author: Simin
This code read images from directory and save their name and labels in .csv file.
Note: the last character of images' name show its label
"""
from imutils import paths
import pandas as pd
Labels=[]
imagePaths =list(paths.list_images("1000testpic"))
for i in imagePaths:
   index=i.find('_')
   label=i[index+1]
   Labels.append(label)
raw_data={'ImageDirectory':imagePaths,
          'label':Labels}
df = pd.DataFrame(raw_data,columns = ['ImageDirectory', 'label'])
df.to_csv('my_csv.csv')
print("Writing complete")