"""

@author: Simin
This code read images from directory and save their name and labels in .csv file.
Note: the last character of images' name show its label
"""
from imutils import paths
import pandas as pd
import os
Labels=[]
imagePaths =list(paths.list_images("D:\\DeepLearning\\Mnist\\1000testpic"))
ImageNames=[]
count=0;
for i in imagePaths:
   index=i.find('_')
   label=i[index+1]
   Labels.append(label)
   name=i.split("\\")[-1]
   name2=name.replace(".jpg", "")
   ImageNames.append(name2)
   imagePaths[count]=i.replace(name, "")
   count=count+1
raw_data={'ImageDirectory':imagePaths,
          'ImageName':ImageNames,
          'label':Labels}
df = pd.DataFrame(raw_data,columns = ['ImageDirectory','ImageName', 'label'])
df.to_csv('my_csv.csv')
print("Writing complete")